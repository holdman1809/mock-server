//
//  AbstractHandler.swift
//  COpenSSL
//
//  Created by Дмитрий on 29/04/2019.
//

import PerfectHTTP

public protocol AbstractHandler {
    
    
    func process(request: HTTPRequest, response: HTTPResponse)
    
    
    
    
}
