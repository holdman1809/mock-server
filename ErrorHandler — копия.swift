//
//  ErrorHandler.swift
//  COpenSSL
//
//  Created by Дмитрий on 29/04/2019.
//

import PerfectHTTP


public class ErrorHandler: AbstractHandler{
    public func process(request: HTTPRequest, response: HTTPResponse) {
        response.setHeader(.contentType, value: "text/html")
        response.appendBody(string: "{\"error_code\":\"500\", \"text\":\"incorrect input data\"}")
        response.completed()
}
}
