//
//  GetBasket.swift
//  COpenSSL
//
//  Created by Дмитрий on 29/04/2019.
//

import PerfectHTTP

public class GetBasketHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let amount = request.param(name: "amount")
        let countGoods = request.param(name: "countGoods")
        
        let id_product = request.param(name: "id_product")
        let product_name = request.param(name: "product_name")
        let price = request.param(name: "price")
        let quantity = request.param(name: "quantity")

        
        response.setHeader(.contentType, value: "text/html")
        response.appendBody(string: "{\"amount\":\"\(amount ?? "nil")\",\"countGoods\":\"\(countGoods ?? "nil")\", \"contents\":{\"id_product\":\"\(id_product ?? "nil")\",\"product_name\":\"\(product_name ?? "nil")\",\"price\":\"\(price ?? "nil")\",\"quantity\":\"\(quantity ?? "nil")\"}}")
        response.completed()
    }
}

